import commentRepository from "../../data/repositories/comment.repository";
import commetReactionRepository from "../../data/repositories/comment-reaction.repository";

export const create = (userId, comment) =>
    commentRepository.create({
        ...comment,
        userId
    });

export const getCommentById = id => commentRepository.getCommentById(id);

export const setReaction = async (userId, { commentId, isLike = true }) => {
    // define the callback for future use as a promise
    const updateOrDelete = react =>
        react.isLike === isLike
            ? commetReactionRepository.deleteById(react.id)
            : commetReactionRepository.updateById(react.id, { isLike });

    const reaction = await commetReactionRepository.getCommentReaction(
        userId,
        commentId
    );

    const result = reaction
        ? await updateOrDelete(reaction)
        : await commetReactionRepository.create({ userId, commentId, isLike });

    // the result is an integer when an entity is deleted
    return Number.isInteger(result)
        ? {}
        : commetReactionRepository.getCommentReaction(userId, commentId);
};
