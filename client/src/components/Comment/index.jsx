import React, { useState } from "react";
import PropTypes from "prop-types";
import { Comment as CommentUI, Label, Icon } from "semantic-ui-react";
import { connect } from "react-redux";
import moment from "moment";
import { getUserImgLink } from "src/helpers/imageHelper";
import { deleteComment } from "../../containers/Thread/actions";

import styles from "./styles.module.scss";

const Comment = props => {
    const {
        userId,
        id,
        comment: { body, createdAt, user }
    } = props;
    const date = moment(createdAt).fromNow();
    const [likes, likeComment] = useState(0);
    const [dislikes, dislikeComment] = useState(0);
    return (
        <CommentUI className={styles.comment}>
            <CommentUI.Avatar src={getUserImgLink(user.image)} />
            <CommentUI.Content>
                <CommentUI.Author as="a">{user.username}</CommentUI.Author>
                <CommentUI.Metadata>{date}</CommentUI.Metadata>
                <CommentUI.Text>{body}</CommentUI.Text>
            </CommentUI.Content>
            <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => likeComment(likes + 1)}
            >
                <Icon name="thumbs up" />
                {likes}
            </Label>
            <Label
                basic
                size="small"
                as="a"
                className={styles.toolbarBtn}
                onClick={() => dislikeComment(dislikes + 1)}
            >
                <Icon name="thumbs down" />
                {dislikes}
            </Label>
            {userId === user.id ? (
                <Label
                    basic
                    size="medium"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => props.deleteComment(id)}
                >
                    <Icon name="delete" />{" "}
                </Label>
            ) : null}
        </CommentUI>
    );
};

Comment.propTypes = {
    comment: PropTypes.objectOf(PropTypes.any).isRequired
};

const mapStateToProps = rootState => ({
    posts: rootState.posts
});
const mapDispatchToProps = {
    deleteComment
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Comment);
