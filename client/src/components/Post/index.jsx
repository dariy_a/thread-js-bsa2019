import React from "react";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import { Card, Image, Label, Icon } from "semantic-ui-react";
import moment from "moment";
import { showPage, setCurrentPostId } from "../../containers/PostModal/actions";

import styles from "./styles.module.scss";

const Post = ({
    userId,
    post,
    likePost,
    dislikePost,
    toggleExpandedPost,
    sharePost,
    deletePost,
    ...props
}) => {
    const {
        id,
        image,
        body,
        user,
        likeCount,
        dislikeCount,
        commentCount,
        createdAt
    } = post;
    const date = moment(createdAt).fromNow();

    const updatePost = () => {
        props.setCurrentPostId(id);
        props.showPage();
    };
    return (
        <Card style={{ width: "100%" }}>
            {image && <Image src={image.link} wrapped ui={false} />}
            <Card.Content>
                <Card.Meta>
                    <span className="date">
                        posted by {user.username}
                        {" - "}
                        {date}
                    </span>

                    {post.user.id === userId ? (
                        <Card.Meta>
                            <Label
                                basic
                                size="small"
                                as="a"
                                className={styles.toolbarBtn}
                                onClick={updatePost}
                            >
                                <Icon name="edit" />
                            </Label>
                            <Label
                                basic
                                size="medium"
                                as="a"
                                className={styles.toolbarBtn}
                                onClick={() => deletePost(id)}
                            >
                                <Icon name="delete" />
                            </Label>
                        </Card.Meta>
                    ) : null}
                </Card.Meta>

                <Card.Description>{body}</Card.Description>
            </Card.Content>
            <Card.Content extra>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => likePost(id)}
                >
                    <Icon name="thumbs up" />
                    {likeCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => dislikePost(id)}
                >
                    <Icon name="thumbs down" />
                    {dislikeCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => toggleExpandedPost(id)}
                >
                    <Icon name="comment" />
                    {commentCount}
                </Label>
                <Label
                    basic
                    size="small"
                    as="a"
                    className={styles.toolbarBtn}
                    onClick={() => sharePost(id)}
                >
                    <Icon name="share alternate" />
                </Label>
            </Card.Content>
        </Card>
    );
};

Post.propTypes = {
    post: PropTypes.objectOf(PropTypes.any).isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    sharePost: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
    posts: rootState.posts,
    isShown: rootState.postModal.isShown
});
const mapDispatchToProps = {
    showPage,
    setCurrentPostId
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Post);
