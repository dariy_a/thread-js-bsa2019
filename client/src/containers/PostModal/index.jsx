import React, { Component } from "react";
import { connect } from "react-redux";
import * as actions from "./actions";
import { updatePost } from "../Thread/actions";
import TextInput from "../../shared/inputs/text/TextInput";
import messagePostConfig from "../../shared/config/messagePostConfig";
import defaultPostConfig from "../../shared/config/defaultPostConfig";
import "bootstrap/dist/css/bootstrap.css";
class PostModal extends Component {
    constructor(props) {
        super(props);
        this.state = this.getDefaultMessageData();
        this.onCancel = this.onCancel.bind(this);
        this.onSave = this.onSave.bind(this);
        this.onChangeData = this.onChangeData.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.postId !== this.props.postId) {
            const post = this.props.posts.find(
                post => post.id === nextProps.postId
            );
            this.setState(post);
        }
    }

    onCancel() {
        this.props.dropCurrentPostId();
        this.props.hidePage();
        this.setState(this.getDefaultMessageData());
    }

    onSave() {
        this.props.updatePost(this.props.postId, this.state);
        this.props.dropCurrentPostId();
        this.props.hidePage();
        this.setState(this.getDefaultMessageData());
    }

    onChangeData(e, keyword) {
        const value = e.target.value;
        this.setState({
            ...this.state,
            [keyword]: value
        });
    }

    getDefaultMessageData() {
        return {
            ...defaultPostConfig
        };
    }

    getInput(data, { label, type, keyword }) {
        switch (type) {
            case "text":
                return (
                    <TextInput
                        label={label}
                        type={type}
                        text={data[keyword]}
                        keyword={keyword}
                        onChange={this.onChangeData}
                    />
                );

            default:
                return null;
        }
    }

    getPostEditPage() {
        const data = this.state;

        return (
            <div
                className="modal"
                style={{ display: "block" }}
                tabIndex="-1"
                role="dialog"
            >
                <div className="modal-dialog" role="document">
                    <div className="modal-content" style={{ padding: "5px" }}>
                        <div className="modal-header">
                            <h5 className="modal-title">Edit post</h5>
                            <button
                                type="button"
                                className="close"
                                data-dismiss="modal"
                                aria-label="Close"
                                onClick={this.onCancel}
                            >
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            {messagePostConfig.map(item =>
                                this.getInput(data, item)
                            )}
                        </div>
                        <div className="modal-footer">
                            <button
                                className="btn btn-secondary"
                                onClick={this.onCancel}
                            >
                                Cancel
                            </button>
                            <button
                                className="btn btn-primary"
                                onClick={this.onSave}
                            >
                                Save
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    render() {
        const isShown = this.props.isShown;
        return isShown ? this.getPostEditPage() : null;
    }
}

const mapStateToProps = rootState => {
    return {
        posts: rootState.posts.posts,
        isShown: rootState.postModal.isShown,
        postId: rootState.postModal.postId
    };
};

const mapDispatchToProps = {
    ...actions,
    updatePost
};

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(PostModal);
