import {
    SET_CURRENT_POST_ID,
    DROP_CURRENT_POST_ID,
    SHOW_PAGE,
    HIDE_PAGE
} from "./actionTypes";

export const setCurrentPostId = id => ({
    type: SET_CURRENT_POST_ID,
    id
});

export const dropCurrentPostId = () => ({
    type: DROP_CURRENT_POST_ID
});

export const showPage = () => ({
    type: SHOW_PAGE
});

export const hidePage = () => ({
    type: HIDE_PAGE
});
