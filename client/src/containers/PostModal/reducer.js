import {
    SET_CURRENT_POST_ID,
    DROP_CURRENT_POST_ID,
    SHOW_PAGE,
    HIDE_PAGE
} from "./actionTypes";

const initialState = {
    postId: "",
    isShown: false
};

export default function(state = initialState, action) {
    switch (action.type) {
        case SET_CURRENT_POST_ID: {
            return {
                ...state,
                postId: action.id
            };
        }
        case DROP_CURRENT_POST_ID: {
            return {
                ...state,
                postId: ""
            };
        }

        case SHOW_PAGE: {
            return {
                ...state,
                isShown: true
            };
        }

        case HIDE_PAGE: {
            return {
                ...state,
                isShown: false
            };
        }

        default:
            return state;
    }
}
