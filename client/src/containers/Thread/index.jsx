import React from "react";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import * as imageService from "src/services/imageService";
import ExpandedPost from "src/containers/ExpandedPost";
import Post from "src/components/Post";
import AddPost from "src/components/AddPost";
import PostModal from "src/containers/PostModal";
import SharedPostLink from "src/components/SharedPostLink";
import { Checkbox, Loader } from "semantic-ui-react";
import InfiniteScroll from "react-infinite-scroller";
import {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    addPost,
    deletePost
} from "./actions";

import styles from "./styles.module.scss";
import { throws } from "assert";

class Thread extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            sharedPostId: undefined,
            showOwnPosts: false,
            showOthersPosts: false,
            showLiked: false
        };
        this.postsFilter = {
            userId: undefined,
            isLike: false,
            from: 0,
            count: 10
        };
    }

    tooglePosts = () => {
        this.setState(
            ({ showOwnPosts }) => ({
                showOwnPosts: !showOwnPosts,
                showOthersPosts: false,
                showLiked: false
            }),

            () => {
                Object.assign(this.postsFilter, {
                    userId: this.state.showOwnPosts
                        ? this.props.userId
                        : undefined,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };
    toogleOthersPosts = () => {
        this.setState(
            ({ showOthersPosts }) => ({
                showOwnPosts: false,
                showOthersPosts: !showOthersPosts,
                showLiked: false
            }),

            () => {
                const users = this.props.posts
                    .filter(post => post.user.id !== this.props.userId)
                    .map(post => {
                        return post.user.id;
                    });
                Object.assign(this.postsFilter, {
                    userId: users,
                    from: 0
                });
                this.props.loadPosts(this.postsFilter);
                this.postsFilter.from = this.postsFilter.count; // for next scroll
            }
        );
    };

    toogleLikedPosts = () => {
        this.setState(({ showLiked }) => ({
            showOwnPosts: false,
            showOthersPosts: false,
            showLiked: !showLiked
        }));
    };
    loadMorePosts = () => {
        this.props.loadMorePosts(this.postsFilter);
        const { from, count } = this.postsFilter;
        this.postsFilter.from = from + count;
    };

    sharePost = sharedPostId => {
        this.setState({ sharedPostId });
    };

    closeSharePost = () => {
        this.setState({ sharedPostId: undefined });
    };

    uploadImage = file => imageService.uploadImage(file);

    render() {
        const { posts = [], expandedPost, hasMorePosts, ...props } = this.props;
        const {
            showOwnPosts,
            showOthersPosts,
            showLiked,
            sharedPostId
        } = this.state;
        return (
            <div className={styles.threadContent}>
                <div className={styles.addPostForm}>
                    <AddPost
                        addPost={props.addPost}
                        uploadImage={this.uploadImage}
                    />
                </div>

                <div className={styles.toolbar}>
                    <Checkbox
                        toggle
                        label="Show only my posts"
                        checked={showOwnPosts}
                        onChange={this.tooglePosts}
                    />
                </div>
                <div className={styles.toolbar}>
                    <Checkbox
                        toggle
                        label="Show only posts of others"
                        checked={showOthersPosts}
                        onChange={this.toogleOthersPosts}
                    />
                </div>

                <div className={styles.toolbar}>
                    <Checkbox
                        toggle
                        label="Show liked posts"
                        checked={showLiked}
                        onChange={this.toogleLikedPosts}
                    />
                </div>
                <InfiniteScroll
                    pageStart={0}
                    loadMore={this.loadMorePosts}
                    hasMore={hasMorePosts}
                    loader={<Loader active inline="centered" key={0} />}
                >
                    <PostModal key={this.props.userId} />
                    {posts.map(post => (
                        <Post
                            userId={this.props.userId}
                            post={post}
                            likePost={props.likePost}
                            dislikePost={props.dislikePost}
                            toggleExpandedPost={props.toggleExpandedPost}
                            sharePost={this.sharePost}
                            deletePost={props.deletePost}
                            key={post.id}
                        />
                    ))}
                </InfiniteScroll>
                {expandedPost && <ExpandedPost sharePost={this.sharePost} />}
                {sharedPostId && (
                    <SharedPostLink
                        postId={sharedPostId}
                        close={this.closeSharePost}
                    />
                )}
            </div>
        );
    }
}

Thread.propTypes = {
    posts: PropTypes.arrayOf(PropTypes.object),
    hasMorePosts: PropTypes.bool,
    expandedPost: PropTypes.objectOf(PropTypes.any),
    sharedPostId: PropTypes.string,
    userId: PropTypes.string,
    loadPosts: PropTypes.func.isRequired,
    loadMorePosts: PropTypes.func.isRequired,
    likePost: PropTypes.func.isRequired,
    dislikePost: PropTypes.func.isRequired,
    toggleExpandedPost: PropTypes.func.isRequired,
    addPost: PropTypes.func.isRequired,
    deletePost: PropTypes.func.isRequired
};

Thread.defaultProps = {
    posts: [],
    hasMorePosts: true,
    expandedPost: undefined,
    sharedPostId: undefined,
    userId: undefined
};

const mapStateToProps = rootState => ({
    posts: rootState.posts.posts,
    hasMorePosts: rootState.posts.hasMorePosts,
    expandedPost: rootState.posts.expandedPost,
    userId: rootState.profile.user.id
});

const actions = {
    loadPosts,
    loadMorePosts,
    likePost,
    dislikePost,
    toggleExpandedPost,
    addPost,
    deletePost
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
    mapStateToProps,
    mapDispatchToProps
)(Thread);
